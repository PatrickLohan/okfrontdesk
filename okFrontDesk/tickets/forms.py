from django import forms
from django.contrib.auth.models import User
from .models import Ticket


class TicketForm(forms.ModelForm):
    description = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = Ticket
        fields = ["description"]


class AssignTicketForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = ["staff"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # only show staff memebers when assigning tickets
        self.fields["staff"].queryset = User.objects.filter(is_staff=True)
