from django.db import models
from users.models import User


class Ticket(models.Model):
    STATUS_CHOICES = [
        ("OPEN", "Open"),
        ("CLOSED", "Closed"),
    ]

    client = models.ForeignKey(User, on_delete=models.CASCADE, related_name="client")  # noqa:E501
    staff = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, related_name="staff"
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=6, choices=STATUS_CHOICES, default="OPEN")  # noqa:E501
    description = models.TextField()

    def __str__(self):
        return f"Ticket{id} - {self.status}"
