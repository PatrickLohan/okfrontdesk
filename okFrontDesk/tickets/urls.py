from django.urls import path
from . import views

app_name = "tickets"

urlpatterns = [
    path("", views.ticket_list, name="ticket_list"),
    path("<int:pk>/", views.ticket_detail, name="ticket_detail"),
    path("new/", views.ticket_new, name="ticket_new"),
    path('<int:pk>/assign/', views.ticket_assign, name='ticket_assign'),
]
